<html>
<body style="width: 400px; margin: 0 auto;">
	<h2>Test RESTful API!</h2>
	
	<div style="background: #eee;">
		<h3>Index all products</h3>
		<b>API</b>: <a href="<%=request.getContextPath()%>/api/product/get-all"><%=request.getContextPath()%>/api/product/get-all</a>
		<hr>
		<b>View</b>: <a href="<%=request.getContextPath()%>/product"><%=request.getContextPath()%>/product</a>
	</div>
	<br>
	<div style="background: #ddd;">
		<h3>View a product</h3>
		<b>API</b>: <a href="<%=request.getContextPath()%>/api/product/get?id=1"><%=request.getContextPath()%>/api/product/get?id=1</a>
		<hr>
		<b>View</b>: <a href="<%=request.getContextPath()%>/product/view?id=1"><%=request.getContextPath()%>/product/view?id=1</a>
	</div>
</body>
</html>

