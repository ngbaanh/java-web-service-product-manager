<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Danh sách sản phẩm</title>
</head>
<body>
	<table border="1" id="ProductTable">
		<caption>Danh sách sản phẩm</caption>
		<tr>
			<th>Mã Số</th>
			<th>Tên Sản Phẩm</th>
			<th>Danh Mục</th>
			<th>Thao tác</th>
		</tr>
	</table>

	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script>
		$(document).ready(function() {
			$.ajax({
				url : "http://localhost:8080<%=request.getContextPath()%>/api/product/get-all",
				success : function(json) {
					if (json.tag == "ProductList") {
						for (i = 0; i < json.data.length; i++) {
							var item = json.data[i];
							var productItem = '<tr><td>' + item.id + '</td><td>' 
								+ item.name + '</td><td>' + item.catalog + '</td><td>'
								+ '<a href="http://localhost:8080<%=request.getContextPath()%>/product/view?id=' + item.id + '">View</a></td></tr>';
							$('#ProductTable').append(productItem);
						}
					}
				}
			});
		});
	</script>
</body>
</html>