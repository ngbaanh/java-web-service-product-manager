<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>View Product: </title>
</head>
<body>
	<table border="0">
		<tr><td>Product ID:</td><td id="ProductId"></td></tr>
		<tr><td>Product Name:</td><td id="ProductName"></td></tr>
		<tr><td>Catalog:</td><td id="Catalog"></td></tr>
	</table>
	
	<%
		int id = Integer.parseInt(request.getAttribute("id").toString());
	%>	
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script>
		$(document).ready(function() {
			$.ajax({
				url : "http://localhost:8080<%=request.getContextPath()%>/api/product/get?id=<%=id%>",
				success : function(json) {
					if (json.tag == "product") {
						$('#ProductName').html(json.productName);
						$('#ProductId').html(json.productId);
						$('#Catalog').html(json.catalog);
					}
				}
			});
		});
	</script>
</body>
</html>