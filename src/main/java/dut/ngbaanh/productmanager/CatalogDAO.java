/**
 * 
 */
package dut.ngbaanh.productmanager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author ngbaanh
 *
 */
public class CatalogDAO extends Database {

	public CatalogDAO() {
		super(); 
	}
	
	public Catalog getCatalog(int cataId) {
		if (cataId <= 0) {
			return null;
		}
		
		try {
			Catalog cata = null; 
			String sql = "SELECT * FROM Catalog where CataId = ?";
			PreparedStatement ps = this.connection.prepareStatement(sql);
			ps.setInt(1, cataId);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				cata = new Catalog(cataId, rs.getString("CataName"));
			}
			return cata;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
