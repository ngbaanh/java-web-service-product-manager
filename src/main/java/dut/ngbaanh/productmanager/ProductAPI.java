package dut.ngbaanh.productmanager;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

//Path: http://localhost/<appln-folder-name>/login
@Path("/product")
public class ProductAPI {
	ProductDAO productDAO;
	
	@GET @Path("/get") @Produces(MediaType.APPLICATION_JSON)
	public String getProduct(@QueryParam("id") int productId) {
		productDAO = new ProductDAO();
		Product product = productDAO.getProduct(productId);
		JSONObject obj = new JSONObject();
		try {
			if (product != null) {
				obj.put("tag", "product");
				obj.put("productId", product.getProductId());
				obj.put("productName", product.getProductName());
				obj.put("catalog", product.getCatalog().getCataName());
			} else {
				obj.put("tag", "product");
				obj.put("message", "Product not found");
			}
			return obj.toString();
		} catch (JSONException e) {
			
		}
		return null;
	}
	
	@GET @Path("/get-all") @Produces(MediaType.APPLICATION_JSON)
	public String getAllProducts() {
		productDAO = new ProductDAO();
		ArrayList<Product> pList = productDAO.getAllProducts();
		JSONObject jsObj = new JSONObject();
		try {
			jsObj.put("tag", "ProductList");
			if (!pList.isEmpty()) {
				JSONArray jsArray = new JSONArray();
				for (Product p : pList) {
					JSONObject jsProduct = new JSONObject();
					jsProduct.put("id", p.getProductId());
					jsProduct.put("name", p.getProductName());
					jsProduct.put("catalog", p.getCatalog().getCataName());
					jsArray.put(jsProduct);
				}
				jsObj.put("data", jsArray);
				
			} else {
				jsObj.put("tag", "product");
				jsObj.put("message", "No product found");
			}
			
		} catch (JSONException e) {
			
		}
		return jsObj.toString();
	}
}
