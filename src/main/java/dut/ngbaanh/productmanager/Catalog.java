package dut.ngbaanh.productmanager;

public class Catalog {
	int cataId;
	String cataName;
	public Catalog(int cataId, String cataName) {
		super();
		this.cataId = cataId;
		this.cataName = cataName;
	}
	public Catalog() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getCataId() {
		return cataId;
	}
	public void setCataId(int cataId) {
		this.cataId = cataId;
	}
	public String getCataName() {
		return cataName;
	}
	public void setCataName(String cataName) {
		this.cataName = cataName;
	}
	
	
}
