/**
 * 
 */
package dut.ngbaanh.productmanager;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author ngbaanh
 *
 */
public class Database {
	Connection connection;
	/**
	 * 
	 */
	public Database() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String address = "jdbc:mysql://localhost:3306/dbProductManager?useUnicode=true&characterEncoding=utf-8";
			//String address = "jdbc:mysql://127.6.73.2:3306/se23?useUnicode=true&characterEncoding=utf-8";
			connection = DriverManager.getConnection(address,"root","root");
			//connection = DriverManager.getConnection(address,"adminMwGeTFy","3smXm8ppTMM5");
			//String url = "jdbc:mysql://127.6.73.2:3306/se23?useUnicode=true&characterEncoding=utf-8";
			//connection = DriverManager.getConnection(url,"adminMwGeTFy","3smXm8ppTMM5");
		} catch (Exception e) {
			System.err.println("[Database constructor] Loi: " + e);
		}
	}

}
