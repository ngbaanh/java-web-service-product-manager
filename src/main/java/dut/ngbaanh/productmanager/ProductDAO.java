/**
 * 
 */
package dut.ngbaanh.productmanager;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * @author ngbaanh
 *
 */
public class ProductDAO extends Database {
	
	public ProductDAO() {
		super(); 
	}
	
	public Product getProduct(int productId) {
		if (productId <= 0) {
			return null;
		}
		
		try {
			Product product = null;
			String sql = "SELECT * FROM Product where ProductId = ?";
			PreparedStatement ps = this.connection.prepareStatement(sql);
			ps.setInt(1, productId);
			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				product = new Product();
				product.setProductId(productId);
				product.setProductName(rs.getString("ProductName"));
				product.setCatalog(new CatalogDAO().getCatalog(rs.getInt("CataId")));
			}
			return product;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public ArrayList<Product> getAllProducts() {
		try {
			ArrayList<Product> pList = new ArrayList<Product>();
			String sql = "SELECT * FROM Product";
			PreparedStatement ps = this.connection.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while  (rs.next()) {
				Product product = new Product();
				product.setProductId(rs.getInt("ProductId"));
				product.setProductName(rs.getString("ProductName"));
				product.setCatalog(new CatalogDAO().getCatalog(rs.getInt("CataId")));
				pList.add(product);
			}
			return pList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

}
