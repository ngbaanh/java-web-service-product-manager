package dut.ngbaanh.productmanager;

public class Product {
	int productId;
	String productName;
	Catalog catalog;
	
	public Product() {
		// TODO Auto-generated constructor stub
	}

	public Product(int produuctId, String productName, Catalog catalog) {
		super();
		this.productId = produuctId;
		this.productName = productName;
		this.catalog = catalog;
	}

	public int getProductId() {
		return productId;
	}

	public void setProductId(int produuctId) {
		this.productId = produuctId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public Catalog getCatalog() {
		return catalog;
	}

	public void setCatalog(Catalog catalog) {
		this.catalog = catalog;
	}
	

}
